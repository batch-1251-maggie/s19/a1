let snorlax = new Pokemon("Snorlax", 5, 49);
let onix = new Pokemon("Onix", 6, 60);
let mew = new Pokemon("Mew", 7, 70);

	function Pokemon(name, lvl, hp){
	this.name = name;
	this.level = lvl;
	this.health = hp;
	this.attack = 10;

	this.tackle = function(target){
		console.log(this)
		console.log(target)

		if (target.health >= 10){
			console.log(`${this.name} tackled ${target.name}`);

			console.log(`${target.name}'s health is now reduced to 
				${target.health - this.attack}`);
			target.health = target.health - this.attack;

		} else if (target.health < 10){
			console.log(`${this.name} tackled ${target.name}`);
			console.log(`${target.name} fainted`);
		}
		};
	}

	// console.log(pikachu);
	// console.log(charizard);
	console.log(snorlax.tackle(onix));
	console.log(snorlax.tackle(onix));
	console.log(snorlax.tackle(mew));
	console.log(snorlax.tackle(mew));
	console.log(onix.tackle(snorlax));
	console.log(mew.tackle(snorlax));
	console.log(mew.tackle(snorlax));
	console.log(onix.tackle(snorlax));
	console.log(onix.tackle(snorlax));

